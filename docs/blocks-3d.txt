-- Hoogle documentation, generated by Haddock
-- See Hoogle, http://www.haskell.org/hoogle/


@package blocks-3d
@version 0.1.0.0

module Blocks.Blocks3d.Types
data Parity
ParityEven :: Parity
ParityOdd :: Parity
parityFromIntegral :: Integral a => a -> Parity
flipParity :: Parity -> Parity

-- | A type for the arguments to blocks_3d that affect the content of the
--   output (as opposed to num-threads which only affects how the output
--   happens).
--   
--   Here, the type <tt>j</tt> of <a>jInternal</a> should be 'Set
--   HalfInteger' when passed to blocks_3d, but <a>HalfInteger</a> when
--   referring to a specific block table written by blocks_3d. The Functor
--   instance lets us access the jInternal field.
data BlockTableKey' j
BlockTableKey :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger) -> j -> HalfInteger -> HalfInteger -> Rendered Rational -> Rendered Rational -> Rendered Rational -> (HalfInteger, HalfInteger, HalfInteger, HalfInteger) -> Sign 'YDir -> Int -> Int -> Set Coordinate -> Int -> Int -> BlockTableKey' j
[jExternal] :: BlockTableKey' j -> (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
[jInternal] :: BlockTableKey' j -> j
[j12] :: BlockTableKey' j -> HalfInteger
[j43] :: BlockTableKey' j -> HalfInteger
[delta12] :: BlockTableKey' j -> Rendered Rational
[delta43] :: BlockTableKey' j -> Rendered Rational
[delta1Plus2] :: BlockTableKey' j -> Rendered Rational
[fourPtStruct] :: BlockTableKey' j -> (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
[fourPtSign] :: BlockTableKey' j -> Sign 'YDir
[order] :: BlockTableKey' j -> Int
[lambda] :: BlockTableKey' j -> Int
[coordinates] :: BlockTableKey' j -> Set Coordinate
[keptPoleOrder] :: BlockTableKey' j -> Int
[precision] :: BlockTableKey' j -> Int
type BlockTableKey = BlockTableKey' HalfInteger
instance Data.Binary.Class.Binary Blocks.Blocks3d.Types.Parity
instance GHC.Generics.Generic Blocks.Blocks3d.Types.Parity
instance GHC.Enum.Enum Blocks.Blocks3d.Types.Parity
instance GHC.Enum.Bounded Blocks.Blocks3d.Types.Parity
instance GHC.Show.Show Blocks.Blocks3d.Types.Parity
instance GHC.Classes.Ord Blocks.Blocks3d.Types.Parity
instance GHC.Classes.Eq Blocks.Blocks3d.Types.Parity
instance GHC.Base.Functor Blocks.Blocks3d.Types.BlockTableKey'
instance Data.Aeson.Types.ToJSON.ToJSON j => Data.Aeson.Types.ToJSON.ToJSON (Blocks.Blocks3d.Types.BlockTableKey' j)
instance Data.Aeson.Types.FromJSON.FromJSON j => Data.Aeson.Types.FromJSON.FromJSON (Blocks.Blocks3d.Types.BlockTableKey' j)
instance Data.Binary.Class.Binary j => Data.Binary.Class.Binary (Blocks.Blocks3d.Types.BlockTableKey' j)
instance GHC.Generics.Generic (Blocks.Blocks3d.Types.BlockTableKey' j)
instance GHC.Show.Show j => GHC.Show.Show (Blocks.Blocks3d.Types.BlockTableKey' j)
instance GHC.Classes.Ord j => GHC.Classes.Ord (Blocks.Blocks3d.Types.BlockTableKey' j)
instance GHC.Classes.Eq j => GHC.Classes.Eq (Blocks.Blocks3d.Types.BlockTableKey' j)
instance (Data.Typeable.Internal.Typeable j, Hyperion.Static.Class.Static (Data.Binary.Class.Binary j)) => Hyperion.Static.Class.Static (Data.Binary.Class.Binary (Blocks.Blocks3d.Types.BlockTableKey' j))
instance Hyperion.Static.Class.Static (Data.Binary.Class.Binary Blocks.Blocks3d.Types.Parity)
instance Data.Aeson.Types.ToJSON.ToJSON Blocks.Blocks3d.Types.Parity
instance Data.Aeson.Types.FromJSON.FromJSON Blocks.Blocks3d.Types.Parity

module Blocks.Blocks3d.WriteTable

-- | Run blocks_3d to generate the blocks corresponding to the given
--   BlockTableKey's
writeBlockTables :: FilePath -> Int -> DebugLevel -> FilePath -> BlockTableKey' (Set HalfInteger) -> IO ()

-- | The file corresponding to a specific value of jInternal
blockTableFilePath :: FilePath -> BlockTableKey' HalfInteger -> FilePath

-- | Collect keys with different jInternal but the same values of all other
--   fields together into a single BlockTableKey' with a set of
--   jInternal's.
groupBlockTableKeys :: Foldable t => t (BlockTableKey' HalfInteger) -> [BlockTableKey' (Set HalfInteger)]
data DebugLevel
NoDebug :: DebugLevel
Debug :: DebugLevel
instance Data.Aeson.Types.ToJSON.ToJSON Blocks.Blocks3d.WriteTable.DebugLevel
instance Data.Aeson.Types.FromJSON.FromJSON Blocks.Blocks3d.WriteTable.DebugLevel
instance Data.Binary.Class.Binary Blocks.Blocks3d.WriteTable.DebugLevel
instance GHC.Generics.Generic Blocks.Blocks3d.WriteTable.DebugLevel
instance GHC.Show.Show Blocks.Blocks3d.WriteTable.DebugLevel
instance GHC.Classes.Ord Blocks.Blocks3d.WriteTable.DebugLevel
instance GHC.Classes.Eq Blocks.Blocks3d.WriteTable.DebugLevel
instance GHC.Base.Semigroup Blocks.Blocks3d.WriteTable.KeySet
instance GHC.Base.Monoid Blocks.Blocks3d.WriteTable.KeySet
instance Hyperion.Static.Class.Static (Data.Binary.Class.Binary Blocks.Blocks3d.WriteTable.DebugLevel)

module Blocks.Blocks3d.ReadTable
data BlockTable a
BlockTable :: Map DerivMatrixKey (DerivMatrix a) -> Rational -> BlockTable a
[$sel:derivMatrices:BlockTable] :: BlockTable a -> Map DerivMatrixKey (DerivMatrix a)
[$sel:deltaMinusX:BlockTable] :: BlockTable a -> Rational
data DerivMatrixKey
DerivMatrixKey :: Parity -> Parity -> Coordinate -> DerivMatrixKey
[$sel:threePtParity120:DerivMatrixKey] :: DerivMatrixKey -> Parity
[$sel:threePtParity430:DerivMatrixKey] :: DerivMatrixKey -> Parity
[$sel:coordinates:DerivMatrixKey] :: DerivMatrixKey -> Coordinate
data DerivMatrix a
DerivMatrix :: Vector HalfInteger -> Vector HalfInteger -> Block3dDR a -> DerivMatrix a
[$sel:j120s:DerivMatrix] :: DerivMatrix a -> Vector HalfInteger
[$sel:j430s:DerivMatrix] :: DerivMatrix a -> Vector HalfInteger
[$sel:derivs:DerivMatrix] :: DerivMatrix a -> Block3dDR a
type DerivMap = Map (Int, Int)
readBlockTable :: (Floating a, Eq a) => FilePath -> BlockTableKey -> IO (BlockTable a)
newtype QuotedFractional a
QuotedFractional :: a -> QuotedFractional a
[$sel:unQuoted:QuotedFractional] :: QuotedFractional a -> a
instance Data.Aeson.Types.FromJSON.FromJSON Blocks.Blocks3d.ReadTable.IndexValues
instance GHC.Generics.Generic Blocks.Blocks3d.ReadTable.IndexValues
instance GHC.Num.Num a => GHC.Num.Num (Blocks.Blocks3d.ReadTable.QuotedFractional a)
instance GHC.Classes.Eq a => GHC.Classes.Eq (Blocks.Blocks3d.ReadTable.QuotedFractional a)
instance (GHC.Real.Fractional a, GHC.Classes.Eq a) => Data.Aeson.Types.FromJSON.FromJSON (Blocks.Blocks3d.ReadTable.DerivDataJSON a)
instance GHC.Generics.Generic (Blocks.Blocks3d.ReadTable.DerivDataJSON a)
instance GHC.Show.Show Blocks.Blocks3d.ReadTable.DerivMatrixKey
instance GHC.Classes.Ord Blocks.Blocks3d.ReadTable.DerivMatrixKey
instance GHC.Classes.Eq Blocks.Blocks3d.ReadTable.DerivMatrixKey
instance GHC.Exception.Type.Exception Blocks.Blocks3d.ReadTable.ReadBlockTableException
instance GHC.Show.Show Blocks.Blocks3d.ReadTable.ReadBlockTableException
instance (GHC.Float.Floating a, GHC.Classes.Eq a) => Data.Aeson.Types.FromJSON.FromJSON (Blocks.Blocks3d.ReadTable.BlockTable a)
instance (GHC.Real.Fractional a, GHC.Classes.Eq a) => Data.Aeson.Types.FromJSON.FromJSON (Blocks.Blocks3d.ReadTable.DerivMapJSON a)
instance (GHC.Real.Fractional a, GHC.Classes.Eq a) => Data.Aeson.Types.FromJSON.FromJSON (Blocks.Blocks3d.ReadTable.PolynomialJSON a)
instance GHC.Real.Fractional a => Data.Aeson.Types.FromJSON.FromJSON (Blocks.Blocks3d.ReadTable.QuotedFractional a)

module Blocks.Blocks3d.Get

-- | Conventions for conformal blocks. For conventions in 1907.11247 (see
--   app. A.5), the following reality properties hold:
--   
--   <ol>
--   <li>OPE coeff are real if all 3 ops are bosonic and pure imaginary if
--   any 2 are fermionic</li>
--   <li>The four-pt functions are real if there are 0 or 4 fermions. They
--   are imaginary if there are 2 fermions.</li>
--   <li>This implies that the conformal blocks are real for bosonic
--   exchanges and imaginary for fermionic exchanges.</li>
--   </ol>
--   
--   We change this convention as follows.
--   
--   1'. OPE coeffs are always real. This is achieved by lambda_there = i
--   lambda_here, when there are 2 fermionic operators. Otherwise
--   lambda_there = lambda_here.
--   
--   2'. Four-pt functions are always real. This is achieved by 4pt_there =
--   i 4pt_here when there are 2 fermionic operators in the 4pt, and
--   4pt_there = 4pt_here otherwise.
--   
--   These relations are nice because they are permutation-invariant. This
--   implies that conformal blocks are always real.
--   
--   The relation between conformal blocks there and conformal blocks here
--   is thus as follows.
--   
--   If there are 0 external fermions, the blocks are equal. If there are 4
--   external fermions, the blocks differ by a sign. If there are 2
--   external ferimions, then
--   
--   Fermionic exchange: block_here = (-1 from 2 3pts) (i^(-1) from 4pt)
--   block_there Bosonic exchange block_here = (i from 1 3pts) (i^(-1) from
--   4pt) block_there
--   
--   blocks_3d computes block_there for bosonic exchange and i^(-1)
--   block_there for fermionic exchange. This means
--   
--   Fermionic exchange: block_here = (-1 from 2 3pts) (i^(-1) from 4pt) (i
--   from block_3d def) blocks_3d = - blocks_3d Bosonic exchange block_here
--   = (i from 1 3pts) (i^(-1) from 4pt) blocks_3d = blocks_3d
--   
--   So it seems we just need to multiply blocks_3d result by -1 for
--   fermionic exchanges or when there are 4 fermions. This is taken care
--   of in getShiftAndBlockVector for blocks loaded from blocks_3d.
--   
--   Identity block generator follows these conventions.
data SO3Struct a1 a2 a3
SO3Struct :: ConformalRep a1 -> ConformalRep a2 -> ConformalRep a3 -> HalfInteger -> HalfInteger -> SO3Struct a1 a2 a3
[operator1] :: SO3Struct a1 a2 a3 -> ConformalRep a1
[operator2] :: SO3Struct a1 a2 a3 -> ConformalRep a2
[operator3] :: SO3Struct a1 a2 a3 -> ConformalRep a3
[structj12] :: SO3Struct a1 a2 a3 -> HalfInteger
[structj123] :: SO3Struct a1 a2 a3 -> HalfInteger
data SO3StructLabel
SO3StructLabel :: HalfInteger -> HalfInteger -> SO3StructLabel
[structj12'] :: SO3StructLabel -> HalfInteger
[structj123'] :: SO3StructLabel -> HalfInteger
data Q4Struct
Q4Struct :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger) -> Sign 'YDir -> Q4Struct
[q4qs] :: Q4Struct -> (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
[q4Sign] :: Q4Struct -> Sign 'YDir
data ConformalRep a
ConformalRep :: a -> HalfInteger -> ConformalRep a
[delta] :: ConformalRep a -> a
[spin] :: ConformalRep a -> HalfInteger
newtype Block3d
Block3d :: Block (SO3Struct Rational Rational Delta) Q4Struct -> Block3d
[unBlock3d] :: Block3d -> Block (SO3Struct Rational Rational Delta) Q4Struct
newtype IdentityBlock3d
IdentityBlock3d :: Block (ConformalRep Rational) Q4Struct -> IdentityBlock3d
[unIdentityBlock3d] :: IdentityBlock3d -> Block (ConformalRep Rational) Q4Struct
data Block3dParams
Block3dParams :: Int -> Int -> Int -> Int -> Block3dParams
[nmax] :: Block3dParams -> Int
[order] :: Block3dParams -> Int
[keptPoleOrder] :: Block3dParams -> Int
[precision] :: Block3dParams -> Int
data Block3dException
Block3dException :: String -> Block3d -> Block3dParams -> Coordinate -> BlockTableKey -> FilePath -> Block3dException
[exceptionMsg] :: Block3dException -> String
[exceptionBlock] :: Block3dException -> Block3d
[exceptionParams] :: Block3dException -> Block3dParams
[exceptionCoord] :: Block3dException -> Coordinate
[exceptionKey] :: Block3dException -> BlockTableKey
[exceptionKeyPath] :: Block3dException -> FilePath
throwBlock3dException :: Coordinate -> Block3dParams -> Block3d -> String -> a

-- | WARNING: This function uses <a>highPrecisionToString</a> on all
--   Rationals, which truncates the precision to a fixed value. We should
--   probably change this.
blockToBlockTableKey :: Coordinate -> Block3dParams -> Block3d -> BlockTableKey
getBlockTable :: forall p m c a. (HasBlocks Block3d p m a, KnownCoordinate c) => Block3d -> Tagged c (Compose (Tagged p) m (BlockTable a))
unitarityBound :: HalfInteger -> Rational
fixedDelta :: ConformalRep Delta -> Rational
threePtParity :: SO3Struct a1 a2 a3 -> Parity
getShiftAndBlockVector :: forall p m a c v. (HasBlocks Block3d p m a, Functor v, Eq a, Fractional a, KnownCoordinate c) => v (Derivative c) -> Block3d -> Compose (Tagged p) m (Rational, DampedRational (FourRhoCrossing a) v a)
getBlock3dContinuum :: (HasBlocks Block3d p m a, Functor v, Floating a, Eq a, KnownCoordinate c) => v (Derivative c) -> Block3d -> Compose (Tagged p) m (DampedRational (FourRhoCrossing a) v a)
getBlock3dIsolated :: (HasBlocks Block3d p m a, Functor v, Foldable v, RealFloat a, KnownCoordinate c) => v (Derivative c) -> Block3d -> Compose (Tagged p) m (v a)
getIdentityBlockZZB :: forall a. Floating a => Int -> IdentityBlock3d -> Map (Derivative 'ZZb) a
instance Data.Aeson.Types.ToJSON.ToJSON Blocks.Blocks3d.Get.SO3StructLabel
instance Data.Aeson.Types.FromJSON.FromJSON Blocks.Blocks3d.Get.SO3StructLabel
instance Data.Binary.Class.Binary Blocks.Blocks3d.Get.SO3StructLabel
instance GHC.Generics.Generic Blocks.Blocks3d.Get.SO3StructLabel
instance GHC.Show.Show Blocks.Blocks3d.Get.SO3StructLabel
instance GHC.Classes.Ord Blocks.Blocks3d.Get.SO3StructLabel
instance GHC.Classes.Eq Blocks.Blocks3d.Get.SO3StructLabel
instance Data.Aeson.Types.ToJSON.ToJSON Blocks.Blocks3d.Get.Q4Struct
instance Data.Aeson.Types.FromJSON.FromJSON Blocks.Blocks3d.Get.Q4Struct
instance Data.Binary.Class.Binary Blocks.Blocks3d.Get.Q4Struct
instance GHC.Generics.Generic Blocks.Blocks3d.Get.Q4Struct
instance GHC.Show.Show Blocks.Blocks3d.Get.Q4Struct
instance GHC.Classes.Ord Blocks.Blocks3d.Get.Q4Struct
instance GHC.Classes.Eq Blocks.Blocks3d.Get.Q4Struct
instance Data.Aeson.Types.ToJSON.ToJSON a => Data.Aeson.Types.ToJSON.ToJSON (Blocks.Blocks3d.Get.ConformalRep a)
instance Data.Aeson.Types.FromJSON.FromJSON a => Data.Aeson.Types.FromJSON.FromJSON (Blocks.Blocks3d.Get.ConformalRep a)
instance Data.Binary.Class.Binary a => Data.Binary.Class.Binary (Blocks.Blocks3d.Get.ConformalRep a)
instance GHC.Generics.Generic (Blocks.Blocks3d.Get.ConformalRep a)
instance GHC.Show.Show a => GHC.Show.Show (Blocks.Blocks3d.Get.ConformalRep a)
instance GHC.Classes.Ord a => GHC.Classes.Ord (Blocks.Blocks3d.Get.ConformalRep a)
instance GHC.Classes.Eq a => GHC.Classes.Eq (Blocks.Blocks3d.Get.ConformalRep a)
instance (Data.Aeson.Types.ToJSON.ToJSON a3, Data.Aeson.Types.ToJSON.ToJSON a2, Data.Aeson.Types.ToJSON.ToJSON a1) => Data.Aeson.Types.ToJSON.ToJSON (Blocks.Blocks3d.Get.SO3Struct a1 a2 a3)
instance (Data.Aeson.Types.FromJSON.FromJSON a1, Data.Aeson.Types.FromJSON.FromJSON a2, Data.Aeson.Types.FromJSON.FromJSON a3) => Data.Aeson.Types.FromJSON.FromJSON (Blocks.Blocks3d.Get.SO3Struct a1 a2 a3)
instance (Data.Binary.Class.Binary a1, Data.Binary.Class.Binary a2, Data.Binary.Class.Binary a3) => Data.Binary.Class.Binary (Blocks.Blocks3d.Get.SO3Struct a1 a2 a3)
instance GHC.Generics.Generic (Blocks.Blocks3d.Get.SO3Struct a1 a2 a3)
instance (GHC.Show.Show a1, GHC.Show.Show a2, GHC.Show.Show a3) => GHC.Show.Show (Blocks.Blocks3d.Get.SO3Struct a1 a2 a3)
instance (GHC.Classes.Ord a1, GHC.Classes.Ord a2, GHC.Classes.Ord a3) => GHC.Classes.Ord (Blocks.Blocks3d.Get.SO3Struct a1 a2 a3)
instance (GHC.Classes.Eq a1, GHC.Classes.Eq a2, GHC.Classes.Eq a3) => GHC.Classes.Eq (Blocks.Blocks3d.Get.SO3Struct a1 a2 a3)
instance Data.Binary.Class.Binary Blocks.Blocks3d.Get.Block3d
instance GHC.Generics.Generic Blocks.Blocks3d.Get.Block3d
instance GHC.Show.Show Blocks.Blocks3d.Get.Block3d
instance GHC.Classes.Ord Blocks.Blocks3d.Get.Block3d
instance GHC.Classes.Eq Blocks.Blocks3d.Get.Block3d
instance Data.Binary.Class.Binary Blocks.Blocks3d.Get.IdentityBlock3d
instance GHC.Generics.Generic Blocks.Blocks3d.Get.IdentityBlock3d
instance GHC.Show.Show Blocks.Blocks3d.Get.IdentityBlock3d
instance GHC.Classes.Ord Blocks.Blocks3d.Get.IdentityBlock3d
instance GHC.Classes.Eq Blocks.Blocks3d.Get.IdentityBlock3d
instance Data.Aeson.Types.FromJSON.FromJSON Blocks.Blocks3d.Get.Block3dParams
instance Data.Aeson.Types.ToJSON.ToJSON Blocks.Blocks3d.Get.Block3dParams
instance Data.Binary.Class.Binary Blocks.Blocks3d.Get.Block3dParams
instance GHC.Generics.Generic Blocks.Blocks3d.Get.Block3dParams
instance GHC.Show.Show Blocks.Blocks3d.Get.Block3dParams
instance GHC.Classes.Ord Blocks.Blocks3d.Get.Block3dParams
instance GHC.Classes.Eq Blocks.Blocks3d.Get.Block3dParams
instance GHC.Exception.Type.Exception Blocks.Blocks3d.Get.Block3dException
instance GHC.Show.Show Blocks.Blocks3d.Get.Block3dException
instance forall k a (c :: k). (GHC.Classes.Eq a, GHC.Float.RealFloat a, Blocks.Coordinate.KnownCoordinate c) => Blocks.HasBlocks.IsolatedBlock Blocks.Blocks3d.Get.IdentityBlock3d (Blocks.Coordinate.Derivative c) a
instance forall k a (c :: k). (GHC.Classes.Eq a, GHC.Float.RealFloat a, Blocks.Coordinate.KnownCoordinate c) => Blocks.HasBlocks.IsolatedBlock Blocks.Blocks3d.Get.Block3d (Blocks.Coordinate.Derivative c) a
instance forall k a (c :: k). (GHC.Classes.Eq a, GHC.Float.RealFloat a, Blocks.Coordinate.KnownCoordinate c) => Blocks.HasBlocks.ContinuumBlock Blocks.Blocks3d.Get.Block3d (Blocks.Coordinate.Derivative c) a
instance Bootstrap.Bounds.Crossing.CrossingEquations.ThreePointStructure (Blocks.Blocks3d.Get.SO3Struct a a a') Blocks.Blocks3d.Get.SO3StructLabel (Blocks.Blocks3d.Get.ConformalRep a) (Blocks.Blocks3d.Get.ConformalRep a')

module Blocks.Blocks3d
type BlockTableKey = BlockTableKey' HalfInteger
data Parity
ParityEven :: Parity
ParityOdd :: Parity
data DebugLevel
NoDebug :: DebugLevel
Debug :: DebugLevel

-- | The file corresponding to a specific value of jInternal
blockTableFilePath :: FilePath -> BlockTableKey' HalfInteger -> FilePath

-- | Run blocks_3d to generate the blocks corresponding to the given
--   BlockTableKey's
writeBlockTables :: FilePath -> Int -> DebugLevel -> FilePath -> BlockTableKey' (Set HalfInteger) -> IO ()

-- | Collect keys with different jInternal but the same values of all other
--   fields together into a single BlockTableKey' with a set of
--   jInternal's.
groupBlockTableKeys :: Foldable t => t (BlockTableKey' HalfInteger) -> [BlockTableKey' (Set HalfInteger)]
data BlockTable a
BlockTable :: Map DerivMatrixKey (DerivMatrix a) -> Rational -> BlockTable a
[$sel:derivMatrices:BlockTable] :: BlockTable a -> Map DerivMatrixKey (DerivMatrix a)
[$sel:deltaMinusX:BlockTable] :: BlockTable a -> Rational
data DerivMatrixKey
DerivMatrixKey :: Parity -> Parity -> Coordinate -> DerivMatrixKey
[$sel:threePtParity120:DerivMatrixKey] :: DerivMatrixKey -> Parity
[$sel:threePtParity430:DerivMatrixKey] :: DerivMatrixKey -> Parity
[$sel:coordinates:DerivMatrixKey] :: DerivMatrixKey -> Coordinate
data DerivMatrix a
DerivMatrix :: Vector HalfInteger -> Vector HalfInteger -> Block3dDR a -> DerivMatrix a
[$sel:j120s:DerivMatrix] :: DerivMatrix a -> Vector HalfInteger
[$sel:j430s:DerivMatrix] :: DerivMatrix a -> Vector HalfInteger
[$sel:derivs:DerivMatrix] :: DerivMatrix a -> Block3dDR a
newtype QuotedFractional a
QuotedFractional :: a -> QuotedFractional a
[$sel:unQuoted:QuotedFractional] :: QuotedFractional a -> a
type DerivMap = Map (Int, Int)
readBlockTable :: (Floating a, Eq a) => FilePath -> BlockTableKey -> IO (BlockTable a)
data Block3dException
Block3dException :: String -> Block3d -> Block3dParams -> Coordinate -> BlockTableKey -> FilePath -> Block3dException
[exceptionMsg] :: Block3dException -> String
[exceptionBlock] :: Block3dException -> Block3d
[exceptionParams] :: Block3dException -> Block3dParams
[exceptionCoord] :: Block3dException -> Coordinate
[exceptionKey] :: Block3dException -> BlockTableKey
[exceptionKeyPath] :: Block3dException -> FilePath
data Block3dParams
Block3dParams :: Int -> Int -> Int -> Int -> Block3dParams
[nmax] :: Block3dParams -> Int
[order] :: Block3dParams -> Int
[keptPoleOrder] :: Block3dParams -> Int
[precision] :: Block3dParams -> Int
newtype IdentityBlock3d
IdentityBlock3d :: Block (ConformalRep Rational) Q4Struct -> IdentityBlock3d
[unIdentityBlock3d] :: IdentityBlock3d -> Block (ConformalRep Rational) Q4Struct
newtype Block3d
Block3d :: Block (SO3Struct Rational Rational Delta) Q4Struct -> Block3d
[unBlock3d] :: Block3d -> Block (SO3Struct Rational Rational Delta) Q4Struct
data ConformalRep a
ConformalRep :: a -> HalfInteger -> ConformalRep a
[delta] :: ConformalRep a -> a
[spin] :: ConformalRep a -> HalfInteger
data Q4Struct
Q4Struct :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger) -> Sign 'YDir -> Q4Struct
[q4qs] :: Q4Struct -> (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
[q4Sign] :: Q4Struct -> Sign 'YDir
data SO3StructLabel
SO3StructLabel :: HalfInteger -> HalfInteger -> SO3StructLabel
[structj12'] :: SO3StructLabel -> HalfInteger
[structj123'] :: SO3StructLabel -> HalfInteger

-- | Conventions for conformal blocks. For conventions in 1907.11247 (see
--   app. A.5), the following reality properties hold:
--   
--   <ol>
--   <li>OPE coeff are real if all 3 ops are bosonic and pure imaginary if
--   any 2 are fermionic</li>
--   <li>The four-pt functions are real if there are 0 or 4 fermions. They
--   are imaginary if there are 2 fermions.</li>
--   <li>This implies that the conformal blocks are real for bosonic
--   exchanges and imaginary for fermionic exchanges.</li>
--   </ol>
--   
--   We change this convention as follows.
--   
--   1'. OPE coeffs are always real. This is achieved by lambda_there = i
--   lambda_here, when there are 2 fermionic operators. Otherwise
--   lambda_there = lambda_here.
--   
--   2'. Four-pt functions are always real. This is achieved by 4pt_there =
--   i 4pt_here when there are 2 fermionic operators in the 4pt, and
--   4pt_there = 4pt_here otherwise.
--   
--   These relations are nice because they are permutation-invariant. This
--   implies that conformal blocks are always real.
--   
--   The relation between conformal blocks there and conformal blocks here
--   is thus as follows.
--   
--   If there are 0 external fermions, the blocks are equal. If there are 4
--   external fermions, the blocks differ by a sign. If there are 2
--   external ferimions, then
--   
--   Fermionic exchange: block_here = (-1 from 2 3pts) (i^(-1) from 4pt)
--   block_there Bosonic exchange block_here = (i from 1 3pts) (i^(-1) from
--   4pt) block_there
--   
--   blocks_3d computes block_there for bosonic exchange and i^(-1)
--   block_there for fermionic exchange. This means
--   
--   Fermionic exchange: block_here = (-1 from 2 3pts) (i^(-1) from 4pt) (i
--   from block_3d def) blocks_3d = - blocks_3d Bosonic exchange block_here
--   = (i from 1 3pts) (i^(-1) from 4pt) blocks_3d = blocks_3d
--   
--   So it seems we just need to multiply blocks_3d result by -1 for
--   fermionic exchanges or when there are 4 fermions. This is taken care
--   of in getShiftAndBlockVector for blocks loaded from blocks_3d.
--   
--   Identity block generator follows these conventions.
data SO3Struct a1 a2 a3
SO3Struct :: ConformalRep a1 -> ConformalRep a2 -> ConformalRep a3 -> HalfInteger -> HalfInteger -> SO3Struct a1 a2 a3
[operator1] :: SO3Struct a1 a2 a3 -> ConformalRep a1
[operator2] :: SO3Struct a1 a2 a3 -> ConformalRep a2
[operator3] :: SO3Struct a1 a2 a3 -> ConformalRep a3
[structj12] :: SO3Struct a1 a2 a3 -> HalfInteger
[structj123] :: SO3Struct a1 a2 a3 -> HalfInteger
throwBlock3dException :: Coordinate -> Block3dParams -> Block3d -> String -> a

-- | WARNING: This function uses <a>highPrecisionToString</a> on all
--   Rationals, which truncates the precision to a fixed value. We should
--   probably change this.
blockToBlockTableKey :: Coordinate -> Block3dParams -> Block3d -> BlockTableKey
getBlockTable :: forall p m c a. (HasBlocks Block3d p m a, KnownCoordinate c) => Block3d -> Tagged c (Compose (Tagged p) m (BlockTable a))
unitarityBound :: HalfInteger -> Rational
fixedDelta :: ConformalRep Delta -> Rational
threePtParity :: SO3Struct a1 a2 a3 -> Parity
getShiftAndBlockVector :: forall p m a c v. (HasBlocks Block3d p m a, Functor v, Eq a, Fractional a, KnownCoordinate c) => v (Derivative c) -> Block3d -> Compose (Tagged p) m (Rational, DampedRational (FourRhoCrossing a) v a)
getBlock3dContinuum :: (HasBlocks Block3d p m a, Functor v, Floating a, Eq a, KnownCoordinate c) => v (Derivative c) -> Block3d -> Compose (Tagged p) m (DampedRational (FourRhoCrossing a) v a)
getBlock3dIsolated :: (HasBlocks Block3d p m a, Functor v, Foldable v, RealFloat a, KnownCoordinate c) => v (Derivative c) -> Block3d -> Compose (Tagged p) m (v a)
getIdentityBlockZZB :: forall a. Floating a => Int -> IdentityBlock3d -> Map (Derivative 'ZZb) a
