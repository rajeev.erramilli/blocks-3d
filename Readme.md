blocks-3d
---------------

A library for computing 3d conformal blocks using the C++ program
[blocks_3d](https://gitlab.com/bootstrapcollaboration/blocks_3d).

Documentation
-------------

[Documentation is here](https://davidsd.gitlab.io/blocks-3d/)
