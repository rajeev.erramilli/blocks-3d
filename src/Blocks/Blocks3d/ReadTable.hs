{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module Blocks.Blocks3d.ReadTable
  ( BlockTable (..)
  , DerivMatrixKey (..)
  , DerivMatrix (..)
  , DerivMap
  , readBlockTable
  , QuotedFractional (..)
  ) where

import           Control.Applicative             (ZipList (..))
import           Control.Exception               (Exception, throw)
import           Control.Monad                   (MonadPlus, mzero)
import           Data.Aeson                      (FromJSON (..))
import qualified Data.Aeson                      as Aeson
import           Data.Functor.Compose            (Compose (..))
import           Data.Map.Strict                 (Map)
import qualified Data.Map.Strict                 as Map
import           Data.Matrix                     (Matrix)
import qualified Data.Matrix                     as Matrix
import qualified Data.MultiSet                   as MultiSet
import           Data.Proxy                      (Proxy (..))
import           Data.Reflection                 (Reifies, reflect)
import qualified Data.Text                       as Text
import qualified Data.Text.Read                  as Text
import           Data.Vector                     (Vector)
import           GHC.Generics                    (Generic)
import           Blocks.Blocks3d.Types      (BlockTableKey, Parity)
import           Blocks.Blocks3d.WriteTable (blockTableFilePath)
import           Blocks.Coordinate          (Coordinate)
import           Blocks.ScalarBlocks.Types  (FourRhoCrossing)
import qualified Bootstrap.Math.DampedRational        as DR
import           Bootstrap.Math.HalfInteger           (HalfInteger)
import           Bootstrap.Math.Polynomial            (Polynomial (..),
                                                  mapCoefficients)

type DerivMap = Map (Int, Int)

-- | These record field names match the schema used by blocks_3d
data IndexValues = IndexValues
  { three_pt_parity_120 :: Parity
  , three_pt_parity_430 :: Parity
  -- | j_120 and j_430 are Vectors because they will be used
  -- unmodified below
  , j_120               :: Vector HalfInteger
  , j_430               :: Vector HalfInteger
  -- | coordinate is a list because this is more convenient for
  -- looping in FromJSON (BlockTable a)
  , coordinates          :: [Coordinate]
  } deriving (Generic, FromJSON)

-- | For internal use with DuplicateRecordFields
coordinateIV :: IndexValues -> [Coordinate]
coordinateIV = coordinates

textToFractional :: (MonadPlus m, Fractional a) => Text.Text -> m a
textToFractional t = case Text.rational t of
  Right (x, "") -> return x
  _             -> mzero

newtype QuotedFractional a = QuotedFractional { unQuoted :: a }
  deriving newtype (Eq, Num)

instance Fractional a => FromJSON (QuotedFractional a) where
  parseJSON = Aeson.withText "QuotedFractional" (fmap QuotedFractional . textToFractional)

newtype PolynomialJSON a = PolynomialJSON { toPolynomial :: Polynomial a }

instance (Fractional a, Eq a) => FromJSON (PolynomialJSON a) where
  parseJSON = fmap (PolynomialJSON . mapCoefficients unQuoted) . parseJSON

newtype DerivMapJSON a = DerivMapJSON { toDerivMap :: DerivMap (Polynomial a) }

unNestVectors2 :: [[a]] -> Map.Map (Int,Int) a
unNestVectors2 vecs = Map.fromList $ do
  (i, vecs_i)  <- zip [0..] vecs
  (j, vecs_ij) <- zip [0..] vecs_i
  pure ((i,j), vecs_ij)

instance (Fractional a, Eq a) => FromJSON (DerivMapJSON a) where
  parseJSON =
    fmap (DerivMapJSON . unNestVectors2 . fmap (fmap toPolynomial)) . parseJSON

-- | These record field names match the schema used by blocks_3d
data DerivDataJSON a = DerivDataJSON
  { index_values  :: [IndexValues]
  , derivs        :: [[[[DerivMapJSON a]]]]
  , pole_list_x   :: [Rational]
  , delta_minus_x :: Rational
  } deriving (Generic, FromJSON)

-- | For internal use with DuplicateRecordFields
derivsDDJ :: DerivDataJSON a -> [[[[DerivMapJSON a]]]]
derivsDDJ = derivs

type Block3dDR a = DR.DampedRational (FourRhoCrossing a) (Matrix `Compose` DerivMap) a

data DerivMatrix a = DerivMatrix
  { j120s  :: Vector HalfInteger
  , j430s  :: Vector HalfInteger
  , derivs :: Block3dDR a
  }

data DerivMatrixKey = DerivMatrixKey
  { threePtParity120 :: Parity
  , threePtParity430 :: Parity
  , coordinates       :: Coordinate
  } deriving (Eq, Ord, Show)

data BlockTable a = BlockTable
  { derivMatrices :: Map DerivMatrixKey (DerivMatrix a)
  , deltaMinusX   :: Rational
  }

transposeLists :: [[a]] -> [[a]]
transposeLists = getZipList . traverse ZipList

-- | Given a shift y, numerator N(x) and a list of poles p_i, forms
-- the DampedRational
--
-- b^(x+y) N(x) / prod_i (x - p_i)
--
-- where b is the base of the DampedRational.
dampedRationalWithShift
  :: forall base f a . (Floating a, Eq a, Reifies base a, Functor f)
  => Rational
  -> f (Polynomial a)
  -> [Rational]
  -> DR.DampedRational base f a
dampedRationalWithShift y numerator poles =
  DR.scaleDampedRational expFactor $
  DR.DampedRational numerator (MultiSet.fromList poles)
  where
    b = reflect @base Proxy
    expFactor = exp (fromRational y * log b)

instance (Floating a, Eq a) => FromJSON (BlockTable a) where
  parseJSON v = do
    derivData <- parseJSON v
    pure $
      let dMx = delta_minus_x derivData
      in BlockTable
         { derivMatrices = Map.fromList $ do
             -- derivs derivData is a four-fold nested list of
             -- DerivMapJSON's. The index levels are:
             --  (1) three_pt_parity_index
             --  (2) j_120_index
             --  (3) j_430_index
             --  (4) coordinate_index
             -- The first step is to loop over the three_pt_parity_index
             (indexVals, indexDeriv)  <-
               zip (index_values derivData) (derivsDDJ derivData)
             -- Next, we must bring coordinate_index to the top level, so
             -- we transpose indexDeriv twice and loop over the
             -- coordinates
             (coord, coordIndexDeriv) <- zip (coordinateIV indexVals)
                                         (transposeLists (fmap transposeLists indexDeriv))
             -- Finally, we can form the DerivMatrix from coordIndexDeriv
             pure ( DerivMatrixKey
                    { threePtParity120 = three_pt_parity_120 indexVals
                    , threePtParity430 = three_pt_parity_430 indexVals
                    , coordinates = coord
                    }
                  , DerivMatrix
                    { j120s  = j_120 indexVals
                    , j430s  = j_430 indexVals
                    -- The polynomials in the block tables must be
                    -- multipled by (4 rho)^Delta = (4 rho)^dMx*(4
                    -- rho)^x. The function 'dampedRationalWithShift'
                    -- takes care of multiplying by the extra (4
                    -- rho)^dMx factor.
                    , derivs = dampedRationalWithShift dMx
                               (Compose $ toDerivMap <$> Matrix.fromLists coordIndexDeriv)
                               (pole_list_x derivData)
                    }
                  )
         , deltaMinusX = dMx
         }

data ReadBlockTableException = ReadBlockTableException
  { exceptionKey  :: BlockTableKey
  , exceptionFile :: FilePath
  , exceptionMsg  :: String
  } deriving (Show, Exception)

readBlockTable
  :: (Floating a, Eq a)
  => FilePath
  -> BlockTableKey
  -> IO (BlockTable a)
readBlockTable blockTableDir key =
  either (throw . ReadBlockTableException key file) id <$>
  Aeson.eitherDecodeFileStrict' file
  where
    file = blockTableFilePath blockTableDir key

