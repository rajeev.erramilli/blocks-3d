
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE RecordWildCards       #-}

module Blocks.Blocks3d.StressDict where

import           Blocks                                       (Delta (..), Block (..))
import           Blocks.Blocks3d.Get                          (ConformalRep (..)
                                                              ,fixedDelta
                                                              ,SO3Struct (..)
                                                              ,SO3StructLabel (..)
                                                              ,Q4Struct)
-- import           Blocks.Blocks3d.Stress                       (StressStruct (..)
--                                                               ,StressStructLabel (..))
import           Bootstrap.Math.FreeVect                      (FreeVect, vec, (*^))
import qualified Bootstrap.Math.FreeVect                      as FV
import           Bootstrap.Math.HalfInteger                   (HalfInteger (..))
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           GHC.Generics                           (Generic)

so3 :: (Num a, Eq a) => HalfInteger -> HalfInteger -> FreeVect SO3StructLabel a
so3 j12 j123 = vec (SO3StructLabel j12 j123)

data StressStruct a = StressStruct
  { operator  :: ConformalRep a
  , struct    :: StressStructLabel
  }
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

data StressStructLabel = GenericParityEven { index :: Int, spin :: Int } 
  | GenericParityOdd { spin :: Int } 
  | Spin2ParityEven
  | Spin2ParityOdd
  | ScalarParityEven
  | ScalarParityOdd
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)


-- instance ThreePointStructure (StressStruct a') StressStructLabel (ConformalRep a') where
--   makeStructure _ _ o3 struct =
--     StressStruct o3 struct

-- TODO: modify the functionts below to instead have the FreeVect have a base field of RationalFunctions

newtype StressBlock3d = StressBlock3d
  { unStressBlock3d :: Block (StressStruct Delta) Q4Struct
  } deriving (Eq, Ord, Show, Generic, Binary)

stressToSO3Struct :: (Eq a, Fractional a, Floating a) => StressStruct Delta -> FreeVect (SO3Struct Rational Rational Delta) a
stressToSO3Struct stress@(StressStruct iRep _) = FV.mapBasis labelToStruct (stressToSO3StructLabel stress)
  where
    stressRep = ConformalRep{ delta = 3, spin = 2}
    labelToStruct SO3StructLabel{..} = SO3Struct{ operator1 = stressRep
                                                , operator2 = stressRep
                                                , operator3 = iRep
                                                , structj12 = structj12'
                                                , structj123 = structj123'}

stressToSO3StructLabel :: (Eq a, Fractional a, Floating a) => StressStruct Delta -> FreeVect SO3StructLabel a
stressToSO3StructLabel (StressStruct iRep ScalarParityEven) =
  -(d-5)*(d-3)/(4*(sqrt 5)) *^ so3 0 0
  -d*(d-5)/(2*(sqrt 14))    *^ so3 2 2
  -d*(d+2)/(4*(sqrt 70))    *^ so3 4 4
  where
    d = fromRational $ fixedDelta iRep

stressToSO3StructLabel (StressStruct iRep ScalarParityOdd) =
  -(sqrt $ 2/5)*(d-4) *^ so3 1 1
  -(d+1)/(sqrt 10)    *^ so3 3 3
  where
    d = fromRational $ fixedDelta iRep

stressToSO3StructLabel (StressStruct iRep Spin2ParityEven) =
  2*(sqrt $ 2/15)*(d-5)*(d-3)*(d-2)       *^ so3 0 2
  -4*(sqrt $ 2/5)*(d-5)*(d-3)             *^ so3 1 2
  +(4/(sqrt 105))*(d-5)*(d-3)*(d+1)       *^ so3 2 0
  -(4/7)*(sqrt $ 2/3)*(d+3)*(d-5)**2      *^ so3 2 2
  +(4/7)*(sqrt $ 6/5)*(d-5)*(d-4)*(d+2)   *^ so3 2 4
  +4*(sqrt $ 6/35)*(d-5)*(d+2)            *^ so3 3 2
  -4*(sqrt $ 3/7)*(d-5)*(d+2)             *^ so3 3 4
  +(2/7)*(sqrt $ 2/15)*(d-5)*(d+2)*(d+3)  *^ so3 4 2
  -4/(7*(sqrt 33))*(d+2)*(d*(d-2)-57)     *^ so3 4 4
  +2/(sqrt 231)*(d-6)*(d+2)*(d+4)         *^ so3 4 6
  where
    d = fromRational $ fixedDelta iRep

stressToSO3StructLabel (StressStruct iRep Spin2ParityOdd) =
  (2/5)*(sqrt $ 2/3)*(d-4)*(d-3)  *^ so3 1 1
  -(2/5)*(d-4)*(d-3)              *^ so3 1 3
  -2*(sqrt $ 6/35)*(d-4)          *^ so3 2 1
  +4*(sqrt $ 6/35)*(d-4)          *^ so3 2 3
  -(sqrt $ 3/7)/5*(d-4)*(d+2)     *^ so3 3 1
  +(2/15)*(d*(d-2)-33)            *^ so3 3 3
  -(sqrt $ 2/7)/3*(d-5)*(d+3)     *^ so3 3 5
  -2/(sqrt 21)*(d+3)              *^ so3 4 3
  +2*(sqrt $ 2/21)*(d+3)          *^ so3 4 5
  where
    d = fromRational $ fixedDelta iRep

stressToSO3StructLabel (StressStruct iRep (GenericParityEven 1 jInt)) =
  sqrt (j*(j-1)*(j+1)*(j+2))/(2*sqrt 5)                                                                       *^ so3 0 j'
  -(d-1)*sqrt ((j-1)*(j+2))/sqrt 10                                                                           *^ so3 1 j'
  +1/2*sqrt((3*(j+1)*(j+2))/(7*(2*j-1)*(2*j+1)))*(d-1-j)*(d-1+j)                                              *^ so3 2 (j'-2)
  +sqrt((j-1)*(j+2)*(2*j-1)*(2*j+3))*(3*(d-1)**2-j**2-j)/((2*j-1)*(2*j+3)*sqrt 14)                            *^ so3 2 j'
  +1/2*sqrt((3*j*(j-1))/(7*(2*j+1)*(2*j+3)))*(d-2-j)*(d+j)                                                    *^ so3 2 (j'+2)
  -sqrt((3*(j+2))/((j-2)*(2*j-1)*(2*j+1)))/4*(d-1)*(d-1-j)*(d-1+j)                                            *^ so3 3 (j'-2)
  +(d-1)*(-5*(d-2)*d+3*j*(j+1)-6)/(2*sqrt(10*(2*j-1)*(2*j+3)))                                                *^ so3 3 j'
  +sqrt(3*(j-1))*(d-1)*(-d+j+2)*(d+j)/(4*sqrt((j+3)*(2*j+1)*(2*j+3)))                                         *^ so3 3 (j'+2)
  +sqrt((j+1)*(j+2))*(-d+j-1)*(-d+j+1)*(d+j-3)*(d+j-1)/(8*sqrt((j-3)*(j-2)*(2*j-5)*(2*j-3)*(2*j-1)*(2*j+1)))  *^ so3 4 (j'-4)
  +sqrt(j+2)*(-d+j+1)*(d+j-1)*(-7*(d-2)*d+(j-1)*j-9)/(4*sqrt(7*(j-2)*(2*j-5)*(2*j-1)*(2*j+1)*(2*j+3)))        *^ so3 4 (j'-2)
  +(5*(d-1)**2*(7*(d-2)*d+12)+(-30*(d-2)*d-33)*j**2+3*j**4+6*j**3-6*(5*(d-2)*d+6)*j)/
    (4*sqrt(70*(2*j-3)*(2*j-1)*(2*j+3)*(2*j+5)))                                                              *^ so3 4 j'
  +sqrt(j-1)*(j*(j+3)-7*(d-1)**2)*(-d+j+2)*(d+j)/(4*sqrt(7*(j+3)*(2*j-1)*(2*j+1)*(2*j+3)*(2*j+7)))            *^ so3 4 (j'+2)
  +sqrt(j*(j-1))*(-d+j+2)*(-d+j+4)*(d+j)*(d+j+2)/(8*sqrt((j+3)*(j+4)*(2*j+1)*(2*j+3)*(2*j+5)*(2*j+7)))        *^ so3 4 (j'+4)
  where
    d = fromRational $ fixedDelta iRep
    -- goes to HalfInteger
    j' = fromInteger $ toInteger $ jInt
    -- goes to a
    j = fromInteger $ toInteger $ jInt

stressToSO3StructLabel (StressStruct iRep (GenericParityEven 2 jInt)) =
  -(d**6-9*d**5+4*d**4*(j**2+j+13)-3*d**3*(2*j*(j+1)-19)+d**2*(j*(j+1)*(j**2+j-7)-86)
    -3*d*(j*(j+1)*(j**2+j-16)+40)+(j-3)*(j-2)*j*(j+1)*(j+3)*(j+4))/(4*sqrt(5*(j-1)*j*(j+1)*(j+2)))            *^ so3 0 j'
  +(d-1)*(d**4-6*d**3+48*d+d**2*(j**2+j-7)+j*(j+1)*(j**2+j-18)-3*d*j*(j+1)+72)/(2*sqrt(10*(j-1)*(j+2)))       *^ so3 1 j'
  + (d**5+d**4*(j-5)+d**3*(j*(j-3)-9)+d**2*(j*(j*(j-1)-15)+29)+d*(j*(j*(j**2+j-17)-1)+56)
    +(j-3)*(j-2)*(j+1)*(j+3)*(j+4))*(-d+j+1)*sqrt 3/(4*sqrt(7*(j+1)*(j+2)*(2*j-1)*(2*j+1)))                   *^ so3 2 (j'-2)
  + (d**6-6*d**5-2*d**4*(j**2+j+1)+3*d**3*(3*j*(j+1)+10)+d**2*(1-2*j*(j+1)*(j**2+j-13))
    +3*d*(j**2+j-16)*(j**2+j-1)+(j-3)*(j-2)*(j+3)*(j+4)*(j**2+j-3))/(2*sqrt(14*(j-1)*(j+2)*(2*j-1)*(2*j+3)))  *^ so3 2 j'
  - (d**5-d**4*(j+6)-d**3*(5-j*(j+5))-d**2*(j*(j**2+4*j-10)-42)+d*(j*(j+2)*(j**2+j-16)+40)
    -(j-3)*(j-2)*j*(j+3)*(j+4))*(d+j)*sqrt 3/(4*sqrt(7*j*(j-1)*(2*j+1)*(2*j+3)))                              *^ so3 2 (j'+2)
  + sqrt(3*(j-2))*(d-1)*(d-j-1)*(d**3+d**2*j+d*(j**2+2*j-13)+(j-3)*(j+3)*(j+4))
    /(8*sqrt((j+2)*(2*j-1)*(2*j+1)))                                                                          *^ so3 3 (j'-2)
  - (d-1)*(3*d**4-3*d**3-2*d**2*(j**2+j+18)+d*(j**2+j-66)+3*(j-3)*(j-2)*(j+3)*(j+4))
    /(4*sqrt(10*(2*j+3)*(2*j-1)))                                                                             *^ so3 3 j'
  + sqrt(3*(j+3))*(d-1)*(d+j)*(d**3-d**2*(j+1)+d*(j**2-14)-(j-3)*(j-2)*(j+4))/(8*sqrt((j-2)*(2*j+1)*(2*j+3))) *^ so3 3 (j'+2)
  - sqrt((j-3)*(j-2))*(d-j+1)*(d-j-1)*(d+j-3)*(d+j+3)*(d**2+d+j*(j+5)+4)
    /(16*sqrt((j+1)*(j+2)*(2*j-5)*(2*j-3)*(2*j-1)*(2*j+1)))                                                   *^ so3 4 (j'-4)
  + sqrt(j-2)*(d-j-1)*(d**5+d**4*(j+2)+d**3*(j*(11-6*j)+26)+d**2*(j*(-6*j**2-15*j+62)+106)
    +d*(j**4+15*j**3+32*j**2-155*j-315)+(j-3)*(j+3)*(j+4)*(j**2-j-9))
    /(8*sqrt(7*(j+2)*(2*j-5)*(2*j-1)*(2*j+1)*(2*j+3)))                                                        *^ so3 4 (j'-2)
  - 3*(d**6+d**5-3*d**4*(3*j*(j+1)-11)+d**3*(107-26*j*(j+1))+d**2*(-j*(j+1)*(9*j**2+9*j-173)-706)
    +d*(j*(j+1)*(17*j**2+17*j-282)+1140)+(j-3)*(j-2)*(j+3)*(j+4)*(j**2+j-10))
    /(8*sqrt(70*(2*j-3)*(2*j-1)*(2*j+3)*(2*j+5)))                                                             *^ so3 4 j'
  + sqrt(j+3)*(d+j)*(d**5-d**4*(j-1)-d**3*(6*j**2+23*j-9)+d**2*(6*j**3+3*j**2-74*j+35)
    +d*(-142+178*j-7*j**2-11*j**3+j**4)-(j-3)*(j-2)*(j+4)*(j**2+3*j-7))
    /(8*sqrt(7*(j-1)*(2*j-1)*(2*j+1)*(2*j+3)*(2*j+7)))                                                        *^ so3 4 (j'+2)
  - sqrt((j+3)*(j+4))*(d-j+2)*(d-j-4)*(d+j)*(d+j+2)*(d**2+d+(j-3)*j)
    /(16*sqrt(j*(j-1)*(2*j+1)*(2*j+3)*(2*j+5)*(2*j+7)))                                                       *^ so3 4 (j'+4)
  where
    d = fromRational $ fixedDelta iRep
    -- goes to HalfInteger
    j' = fromInteger $ toInteger $ jInt
    -- goes to a
    j = fromInteger $ toInteger $ jInt

stressToSO3StructLabel (StressStruct _ (GenericParityEven _ _)) = undefined

stressToSO3StructLabel (StressStruct iRep (GenericParityOdd jInt))
  | even jInt = 
    2*sqrt(2/5)*(d-5)*(d-4)*(d-3)/sqrt((j+1)*(j+2))                       *^ so3 1 (j'-1)
    -2*sqrt(2/5)*(d-5)*(d-4)*(d-3)/sqrt(j*(j+2))                          *^ so3 1 (j'+1)
    -2*sqrt(6/7*(j-1)/(j+2))*(d-5)*(d-4)                                  *^ so3 2 (j'-1)
    +2*sqrt(6/7)*(d-5)*(d-4)                                              *^ so3 2 (j'+1)
    +(d-5)*(d-j)*(d+j-2)*sqrt((j-1)*(j-2)/((j+1)*(j+2)*(2*j-3)*(2*j-1)))  *^ so3 3 (j'-3)
    +(d-5)*(-(d-2)*d+5*j**2-12)*sqrt(3/5*(j-1)/((j+2)*(2*j-3)*(2*j+3)))   *^ so3 3 (j'-1)
    +(d-5)*((d-2)*d-5*j*(j+2)+7)*sqrt(3/5/((2*j-1)*(2*j+5)))              *^ so3 3 (j'+1)
    -(d-5)*(d-j-3)*(d+j+1)*sqrt((j+3)/(j*(2*j+3)*(2*j+5)))                *^ so3 3 (j'+3)
    -(d-j)*(d+j-2)*sqrt((j-3)*(j-2)*(j-1)/((j+2)*(2*j-3)*(2*j-1)))        *^ so3 4 (j'-3)
    +(3*(d-2)*d-7*j**2+18)*sqrt((j-2)*(j-1)/(7*(2*j-3)*(2*j+3)))          *^ so3 4 (j'-1)
    +(-3*(d-2)*d+7*j*(j+2)-11)*sqrt((j-1)*(j+3)/(7*(2*j-1)*(2*j+5)))      *^ so3 4 (j'+1) -- | TODO: check sign, note didn't have sign
    +(d-j-3)*(d+j+1)*sqrt((j+3)*(j+4)/((2*j+3)*(2*j+5)))                  *^ so3 4 (j'+3)
  | otherwise = 
    -2*sqrt(2/5*(j-1)*(j+1)*(j+2))                                  *^ so3 1 (j'-1)
    -2*sqrt(2/5*(j-1)*j*(j+2))                                      *^ so3 1 (j'+1)
    +2*sqrt(6/7*(j+2))*(d-1)                                        *^ so3 2 (j'-1)
    +2*sqrt(6/7*(j-1))*(d-1)                                        *^ so3 2 (j'+1)
    -(d-j)*(d+j-2)*sqrt((j+1)*(j+2)/((j-2)*(2*j-3)*(2*j-1)))        *^ so3 3 (j'-3)
    +(-5*(d-2)*d+j**2-6)*sqrt(3*(j+2)/(5*(2*j-3)*(2*j+3)))          *^ so3 3 (j'-1)
    +(j*(j+2)-5*(d-1)**2)*sqrt(3/5*(j-1)/((2*j-1)*(2*j+5)))         *^ so3 3 (j'+1)
    -(d-j-3)*(d+j+1)*sqrt(j*(j-1)/((j+3)*(2*j+3)*(2*j+5)))          *^ so3 3 (j'+3)
    +(d-1)*(d-j)*(d+j-2)*sqrt((j+2)/((j-3)*(j-2)*(2*j-3)*(2*j-1)))  *^ so3 4 (j'-3)
    +(d-1)*(7*(d-2)*d-3*j**2+12)/sqrt(7*(j-2)*(2*j-3)*(2*j+3))      *^ so3 4 (j'-1)
    -(d-1)*(-7*(d-2)*d+3*j*(j+2)-9)/sqrt(7*(j+3)*(2*j-1)*(2*j+5))   *^ so3 4 (j'+1)
    +(d-1)*(d-j-3)*(d+j+1)*sqrt((j-1)/((j+3)*(j+4)*(2*j+3)*(2*j+5)))*^ so3 4 (j'+3)
  where
    d = fromRational $ fixedDelta iRep
    -- goes to HalfInteger
    j' = fromInteger $ toInteger $ jInt
    -- goes to a
    j = fromInteger $ toInteger $ jInt