{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TypeFamilies      #-}

module Blocks.Blocks3d.Build where

import qualified Blocks.Blocks3d                as B3d
import           Blocks.Blocks3d.Types          (BlockTableKey' (..))
import           Blocks.Sign                    (toNum)
import           Bootstrap.Build                (BuildLink (..))
import           Control.Monad                  (void)
import           Control.Monad.IO.Class         (liftIO)
import           Control.Monad.Reader           (asks, local)
import           Data.BinaryHash                (hashBase64Safe)
import           Data.Void                      (Void)
import           Hyperion                       (Job, cAp, cPure, jobNodeCpus,
                                                 mapConcurrently_, ptrAp,
                                                 remoteEval, setTaskCpus)
import           Hyperion.Bootstrap.Bound.Types (BoundConfig (..), BoundFiles,
                                                 SDPFetchValue, blockDir)
import qualified Hyperion.Log                   as Log
import           Hyperion.WorkerCpuPool         (NumCPUs (..))
import           System.Directory               (doesFileExist)
import           System.FilePath.Posix          ((</>))

type instance SDPFetchValue a B3d.StressTableKey = B3d.BlockTable a

-- | TODO: Currently, DebugLevel = Debug is turned on by default for
-- writeBlockTables. This could be changed to give the user more
-- control.
stressBlock3dBuildLink
  :: BoundConfig
  -> BoundFiles
  -> BuildLink Job B3d.StressTableKey Void
stressBlock3dBuildLink config files = BuildLink
  { buildDeps = \B3d.StressTableKey -> [ B3d.BlockTableKey {
      -- TODO
  } ]
  , checkCreated = liftIO . doesFileExist . B3d.blockTableFilePath (blockDir files)
  , buildAll = \keys -> do
      nodeCpus <- asks jobNodeCpus
      mapConcurrently_ (makeStressBlock3d nodeCpus B3d.Debug) (B3d.groupBlockTableKeys keys)
  }
  where
    blocks3dExecutable = scriptsDir config </> "blocks_3d.sh"
    makeStressBlock3d (NumCPUs nodeCpus) debugLevel t = local (setTaskCpus (NumCPUs $ numThreads nodeCpus t)) $ do
      Log.info "Building Blocks" (t, hashBase64Safe (void t))
      remoteEval $ ptrAp (static liftIO) $
        static B3d.writeStressBlockTables `ptrAp`
        cPure blocks3dExecutable `cAp`
        cPure (numThreads nodeCpus t) `cAp`
        cPure debugLevel `cAp`
        cPure (blockDir files) `cAp`
        cPure t
    -- We want to make sure that blocks_3d only gets as many threads as it needs, which for now is approximately equal to nmax.
    numThreads cpus StressTableKey{..}
      | mod lambda 2 == 1 = min ((lambda+1) `div ` 2) cpus
      | otherwise = min ((lambda + 1 + toNum fourPtSign) `div` 2) cpus

