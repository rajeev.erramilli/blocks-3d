{-# LANGUAGE DeriveAnyClass   #-}
{-# LANGUAGE StaticPointers   #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE DeriveGeneric    #-}
{-# LANGUAGE RecordWildCards  #-}
{-# LANGUAGE TypeApplications #-}

module Blocks.Blocks3d.WriteTable
  ( writeBlockTables
  , blockTableFilePath
  , groupBlockTableKeys
  , DebugLevel(..)
  ) where

import Hyperion.Static (Dict(..), Static(..), cPtr)
import           Control.Monad              (void)
import           Data.Aeson                 (FromJSON, ToJSON)
import           Data.Binary                (Binary)
import           Data.BinaryHash            (hashBase64Safe)
import           Data.Foldable              (foldMap')
import           Data.List                  (intercalate)
import qualified Data.Map.Strict            as Map
import           Data.Rendered              (render)
import qualified Data.Set                   as Set
import           GHC.Generics               (Generic)
import           Blocks                (coordinateToString)
import           Blocks.Blocks3d.Types (BlockTableKey' (..))
import qualified Blocks.Sign           as Sign
import           Bootstrap.Math.HalfInteger      (HalfInteger)
import qualified Bootstrap.Math.HalfInteger      as HalfInteger
import           System.FilePath            ((<.>), (</>))
import           System.Process             (callProcess)

-- | The path for a block table takes the form
-- blocks_3d_tables_hash/spin_j.json, where "hash" is the hash of
-- BlockTableKey' with jInternal = (), and "j" is the value of
-- jInternal for that table (filled in by blocks_3d). Using the hash
-- has two disadvantages: (1) There can be collisions. This would be
-- very bad. Currently Hashable targets Int64, so the probability of a
-- collision is pretty small if we have a few thousand block
-- files. (2) The filename doesn't look sensible to humans. However,
-- each file contains its own metadata, so this can be overcome with
-- tooling.
mkBlockTableFilePath :: FilePath -> BlockTableKey' () -> String -> FilePath
mkBlockTableFilePath blockTableDir blockTable jInternalString =
  blockTableDir </> tableName </> "spin_" ++ jInternalString <.> "json"
  where
    tableName = "blocks_3d_tables_" ++ hashBase64Safe blockTable

-- | A file template for passing to blocks_3d
blockTableFileTemplate :: FilePath -> BlockTableKey' j -> FilePath
blockTableFileTemplate blockTableDir blockTable =
  mkBlockTableFilePath blockTableDir (fmap (const ()) blockTable) "{}"

-- | The file corresponding to a specific value of jInternal
blockTableFilePath :: FilePath -> BlockTableKey' HalfInteger -> FilePath
blockTableFilePath blockTableDir blockTable =
  mkBlockTableFilePath blockTableDir (fmap (const ()) blockTable) (showWithDecimal (jInternal blockTable))
  where
    -- | We use showWithDecimal for jInternal instead of show because
    -- blocks_3d outputs integers followed by ".0"
    showWithDecimal j = case HalfInteger.toInteger j of
      Just jInt -> show jInt ++ ".0"
      Nothing   -> show j

data DebugLevel = NoDebug | Debug
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

instance Static (Binary DebugLevel) where closureDict = cPtr (static Dict)

writeBlockTablesArgs :: Int -> DebugLevel -> FilePath -> BlockTableKey' (Set.Set HalfInteger) -> [String]
writeBlockTablesArgs numThreads debugLevel blockTableDir b@BlockTableKey{..} =
  [ "--j-external"     , showj4 jExternal
  , "--j-internal"     , showjList (Set.toList jInternal)
  , "--j-12"           , show j12
  , "--j-43"           , show j43
  , "--delta-12"       , render delta12
  , "--delta-43"       , render delta43
  , "--delta-1-plus-2" , render delta1Plus2
  , "--four-pt-struct" , showj4 fourPtStruct
  , "--four-pt-sign"   , show (Sign.toNum @Int fourPtSign)
  , "--order"          , show order
  , "--lambda"         , show lambda
  , "--coordinates"    , intercalate "," (map coordinateToString (Set.toList coordinates))
  , "--kept-pole-order", show keptPoleOrder
  , "--num-threads"    , show numThreads
  , "--debug"          , case debugLevel of { NoDebug -> "0"; Debug -> "1" }
  , "--precision"      , show precision
  , "-o"               , blockTableFileTemplate blockTableDir b
  ]
  where
    showj4 (j1, j2, j3, j4) = intercalate "," (map show [j1,j2,j3,j4])
    -- TODO: This could result in a large string. Compress using ranges syntax.
    showjList js = intercalate "," (map show js)

-- | Run blocks_3d to generate the blocks corresponding to the given
-- BlockTableKey's
writeStressBlockTables
  :: FilePath    -- ^ Path to the blocks_3d executable
  -> Int         -- ^ Number of threads to use
  -> DebugLevel  -- ^ Whether to print debug output
  -> FilePath    -- ^ Directory for block tables
  -> StressTableKey' (Set.Set HalfInteger) -- ^ Block tables to generate
  -> IO ()
writeStressBlockTables executable numThreads debugLevel blockTableDir b = do
  let args = writeBlockTablesArgs numThreads debugLevel blockTableDir b
  -- Read in blocks 3d: call get or something?
  -- combine the blocks as per the way we designed it in the readTable, except:
  -- should probably call "getBlockTable"
  callProcess executable args

-- | Group keys by all their values except jInternal
newtype KeySet = KeySet (Map.Map (BlockTableKey' ()) (Set.Set HalfInteger))

-- | Union the jInternal values
instance Semigroup KeySet where
  (KeySet ks1) <> (KeySet ks2) = KeySet (Map.unionWith Set.union ks1 ks2)

instance Monoid KeySet where
  mempty = KeySet Map.empty

-- | Collect keys with different jInternal but the same values of all
-- other fields together into a single BlockTableKey' with a set of
-- jInternal's.
groupBlockTableKeys
  :: Foldable t
  => t (BlockTableKey' HalfInteger)
  -> [BlockTableKey' (Set.Set HalfInteger)]
groupBlockTableKeys = fromKeySet . foldMap' toKeySet
  where
    toKeySet k = KeySet (Map.singleton (void k) (Set.singleton (jInternal k)))
    fromKeySet (KeySet ks) = [fmap (const js) k | (k,js) <- Map.toList ks]
