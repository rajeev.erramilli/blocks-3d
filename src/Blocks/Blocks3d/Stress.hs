{-# LANGUAGE ApplicativeDo         #-}
{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE InstanceSigs          #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Blocks.Blocks3d.Stress where

import           Control.Exception                      (Exception, throw)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           Data.Foldable                          (toList)
import qualified Data.Foldable                          as Foldable
import           Data.Functor.Compose                   (Compose (..))
import qualified Data.Map.Strict                        as Map
import qualified Data.Matrix                            as Matrix
import           Data.Maybe                             (fromMaybe)
import           Data.Proxy                             (Proxy (..))
import           Data.Reflection                        (reflect)
import           Data.Rendered                          (mkRendered)
import qualified Data.Set                               as Set
import           Data.Tagged                            (Tagged, untag)
import qualified Data.Vector                            as V
import           GHC.Generics                           (Generic)
import           Blocks                            (Block (..), BlockBase,
                                                         BlockFetchContext,
                                                         BlockTableParams,
                                                         ContinuumBlock (..),
                                                         Coordinate (..),
                                                         CoordinateDir (..),
                                                         Delta (..),
                                                         Derivative (..),
                                                         HasBlocks,
                                                         IsolatedBlock (..),
                                                         KnownCoordinate (..),
                                                         Sign, zzbDerivsAll)
import           Blocks.HasBlocks                        (getBlockFreeVectIsolated, getBlockFreeVectContinuum)
import           Blocks.Blocks3d.Get                          (ConformalRep (..)
                                                              ,SO3StructLabel (..), Q4Struct (..)
                                                              ,SO3Struct (..)
                                                              ,Block3d (..)
                                                              ,Block3dParams (..))
import           Blocks.Blocks3d.ReadTable         (BlockTable (..),
                                                         DerivMatrix (..),
                                                         DerivMatrixKey (..))
import           Blocks.Blocks3d.StressDict               (StressStruct (..)
                                                              ,StressStructLabel (..)
                                                              ,StressBlock3d (..)
                                                              ,stressToSO3Struct)
import           Blocks.Blocks3d.Types             (BlockTableKey, Parity,
                                                         parityFromIntegral)
import qualified Blocks.Blocks3d.Types             as Key
import           Blocks.Blocks3d.WriteTable        (blockTableFilePath)
import           Blocks.ScalarBlocks.Types         (FourRhoCrossing)
import qualified Blocks.Sign                       as Sign
import           Bootstrap.Bounds.Crossing.CrossingEquations (ThreePointStructure (..))
import           Bootstrap.Build                             (Fetches, fetch)
import qualified Bootstrap.Math.DampedRational               as DR
import           Bootstrap.Math.FreeVect                     ( FreeVect (..)
                                                             , multiplyWith
                                                             , evalFreeVectM)
import           Bootstrap.Math.HalfInteger                  (HalfInteger (..),
                                                         fromHalfInteger)
import qualified Bootstrap.Math.HalfInteger                  as HalfInteger
import           Bootstrap.Math.Util                         (choose, pochhammer)
import           Bootstrap.Math.VectorSpace                  (IsBaseField (..))

-- TODO: Is there a principled way to avoid this monstrosity?
import           Blocks.ScalarBlocks.Get           (highPrecisionToString)

{- |

Conventions for conformal blocks. For conventions in 1907.11247 (see
app. A.5), the following reality properties hold:

1. OPE coeff are real if all 3 ops are bosonic and pure imaginary if
   any 2 are fermionic

2. The four-pt functions are real if there are 0 or 4 fermions. They
   are imaginary if there are 2 fermions.

3. This implies that the conformal blocks are real for bosonic
   exchanges and imaginary for fermionic exchanges.

We change this convention as follows.

1'. OPE coeffs are always real. This is achieved by lambda_there = i
   lambda_here, when there are 2 fermionic operators. Otherwise
   lambda_there = lambda_here.

2'. Four-pt functions are always real. This is achieved by 4pt_there =
   i 4pt_here when there are 2 fermionic operators in the 4pt, and
   4pt_there = 4pt_here otherwise.

These relations are nice because they are permutation-invariant. This
implies that conformal blocks are always real.

The relation between conformal blocks there and conformal blocks here
is thus as follows.

If there are 0 external fermions, the blocks are equal. If there are 4
external fermions, the blocks differ by a sign. If there are 2
external ferimions, then

Fermionic exchange:
  block_here = (-1 from 2 3pts) (i^(-1) from 4pt) block_there
Bosonic exchange
  block_here = (i from 1 3pts) (i^(-1) from 4pt) block_there

blocks_3d computes block_there for bosonic exchange and i^(-1)
block_there for fermionic exchange. This means

Fermionic exchange:
  block_here = (-1 from 2 3pts) (i^(-1) from 4pt) (i from block_3d def) blocks_3d = - blocks_3d
Bosonic exchange
  block_here = (i from 1 3pts) (i^(-1) from 4pt) blocks_3d = blocks_3d

So it seems we just need to multiply blocks_3d result by -1 for
fermionic exchanges or when there are 4 fermions. This is taken
care of in getShiftAndBlockVector for blocks loaded from blocks_3d.

Identity block generator follows these conventions.

-}

-- stressToSO3 :: StressBlock3d -> FreeVect Block3d a

getStressBlockIsolated
  :: forall p m a c v. (HasBlocks Block3d p m a, Foldable v, Functor v, Applicative v, Eq a, RealFloat a, KnownCoordinate c, VectorSpace v, IsBaseField v a )
  => v (Derivative c)
  -> StressBlock3d
  -> Compose (Tagged p) m (v a)
getStressBlockIsolated derivVec b'@(StressBlock3d b@Block{..}) = do
  -- (so3struct12, coeff12) <- toList stressToSO3Struct struct12
  -- (so3struct43, coeff34) <- toList stressToSO3Struct struct43
  -- THIS IS JUST MULTIPLYWITH
  -- block3d <- coeff12*coeff34 *^ vec (Block { struct12 = so3struct12, struct43 = so3struct43, 4ptfunctional = ...} )
  -- (cartoon of what needs to happen)
  let
    toBlock :: (SO3Struct Rational Rational Delta) -> (SO3Struct Rational Rational Delta) -> Block3d
    toBlock s12 s43 = Block3d {
      unBlock3d = Block { struct12 = s12, struct43 = s43, ..}
    }
    struct12s = stressToSO3Struct struct12
    struct43s = stressToSO3Struct struct43
    blocks :: FreeVect Block3d a
    blocks = multiplyWith toBlock struct12s struct43s
  block <- evalFreeVectM (getBlockIsolated derivVec) blocks
  return block
  -- evalFreeVect getShiftAndBlockVector block3d
    
  
-- getStressBlockContinuum
--   :: forall p m a c v. (HasBlocks Block3d p m a, Foldable v, Functor v, Applicative v, Eq a, RealFloat a, KnownCoordinate c, VectorSpace v, IsBaseField v a )
--   => v (Derivative c)
--   -> StressBlock3d
--   -> Compose (Tagged p) m (DR.DampedRational (FourRhoCrossing a) v a)
-- getStressBlockContinuum derivVec b'@(StressBlock3d b@Block{..}) = do
--   -- (so3struct12, coeff12) <- toList stressToSO3Struct struct12
--   -- (so3struct43, coeff34) <- toList stressToSO3Struct struct43
--   -- THIS IS JUST MULTIPLYWITH
--   -- block3d <- coeff12*coeff34 *^ vec (Block { struct12 = so3struct12, struct43 = so3struct43, 4ptfunctional = ...} )
--   -- (cartoon of what needs to happen)
--   let
--     blocks :: FreeVect Block3d a
--     blocks = multiplyWith toBlock (stressToSO3Struct struct12) (stressToSO3Struct struct43)
--   block <- evalFreeVectM (getBlockContinuum derivVec) blocks
--   return block
--   -- evalFreeVect getShiftAndBlockVector block3d
--   where
--     toBlock :: (SO3Struct Rational Rational Delta) -> (SO3Struct Rational Rational Delta) -> Block3d
--     toBlock s12 s43 = Block3d {
--       unBlock3d = Block { struct12 = s12, struct43 = s43, ..}
--     }

getStressBlockContinuum
  :: forall p m a c v. (HasBlocks Block3d p m a, Foldable v, Functor v, Applicative v, Eq a, RealFloat a, KnownCoordinate c)
  => v (Derivative c)
  -> StressBlock3d
  -> Compose (Tagged p) m (DR.DampedRational (FourRhoCrossing a) v a)
getStressBlockContinuum derivVec b'@(StressBlock3d b@Block{..}) = do
  -- (so3struct12, coeff12) <- toList stressToSO3Struct struct12
  -- (so3struct43, coeff34) <- toList stressToSO3Struct struct43
  -- THIS IS JUST MULTIPLYWITH
  -- block3d <- coeff12*coeff34 *^ vec (Block { struct12 = so3struct12, struct43 = so3struct43, 4ptfunctional = ...} )
  -- (cartoon of what needs to happen)
  let
    blocks :: FreeVect Block3d a
    blocks = multiplyWith toBlock (stressToSO3Struct struct12) (stressToSO3Struct struct43)
  block <- getBlockFreeVectContinuum derivVec blocks
  return block
  -- evalFreeVect getShiftAndBlockVector block3d
  where
    toBlock :: (SO3Struct Rational Rational Delta) -> (SO3Struct Rational Rational Delta) -> Block3d
    toBlock s12 s43 = Block3d {
      unBlock3d = Block { struct12 = s12, struct43 = s43, ..}
    }

type instance BlockBase StressBlock3d a = FourRhoCrossing a
type instance BlockTableParams StressBlock3d = Block3dParams
type instance BlockFetchContext StressBlock3d a m = Fetches BlockTableKey (BlockTable a) m

instance (Eq a, RealFloat a, KnownCoordinate c) => IsolatedBlock StressBlock3d (Derivative c) a where
  getBlockIsolated 
    :: forall p m a c v. (HasBlocks Block3d p m a, Foldable v, Functor v, Applicative v, Eq a, RealFloat a, KnownCoordinate c, VectorSpace v, IsBaseField v a )
    => v (Derivative c)
    -> StressBlock3d
    -> Compose (Tagged p) m (v a)
  getBlockIsolated = getStressBlockIsolated

instance (Eq a, RealFloat a, KnownCoordinate c) => ContinuumBlock StressBlock3d (Derivative c) a where
  getBlockContinuum 
    :: forall p m a c v. (HasBlocks Block3d p m a, Foldable v, Functor v, Applicative v, Eq a, RealFloat a, KnownCoordinate c, VectorSpace v, IsBaseField v a )
    => v (Derivative c)
    -> StressBlock3d
    -> Compose (Tagged p) m (DR.DampedRational (FourRhoCrossing a) v a)
  getBlockContinuum = getStressBlockContinuum

-- -- | WARNING: This function uses 'highPrecisionToString' on all
-- -- Rationals, which truncates the precision to a fixed value. We
-- -- should probably change this.
-- blockToBlockTableKey :: Coordinate -> Block3dParams -> Block3d -> BlockTableKey
-- blockToBlockTableKey coordinate params block = case (params, block) of
--   ( Block3dParams { nmax, order, keptPoleOrder, precision }
--     , Block3d
--       (Block
--        { struct12         = SO3Struct (ConformalRep delta1 j1) (ConformalRep delta2 j2) (ConformalRep _ j) j12 _
--        , struct43         = SO3Struct (ConformalRep delta4 j4) (ConformalRep delta3 j3) (ConformalRep _ j') j43 _
--        , fourPtFunctional = Q4Struct qs s
--        })
--     ) -> if j == j' then
--     Key.BlockTableKey
--     { Key.jExternal     = (j1, j2, j3, j4)
--     , Key.jInternal     = j
--     , Key.j12           = j12
--     , Key.j43           = j43
--     , Key.delta12       = mkRendered highPrecisionToString $ delta1 - delta2
--     , Key.delta43       = mkRendered highPrecisionToString $ delta4 - delta3
--     , Key.delta1Plus2   = mkRendered highPrecisionToString $ delta1 + delta2
--     , Key.fourPtStruct  = qs
--     , Key.fourPtSign    = s
--     , Key.order         = order
--     , Key.lambda        = 2*nmax - 1
--     , Key.coordinates   = Set.singleton coordinate
--     , Key.keptPoleOrder = keptPoleOrder
--     , Key.precision     = precision
--     }
--     else error $ "Mismatched internal operators in block " ++ show block

-- getStressBlock3dIsolated
--   :: (HasBlocks StressBlock3d p m a, Functor v, Foldable v, RealFloat a, KnownCoordinate c)
--   => v (Derivative c)
--   -> StressBlock3d
--   -> Compose (Tagged p) m (v a)
-- getStressBlock3dIsolated derivVec (StressBlock3d b) = do

--   where
    