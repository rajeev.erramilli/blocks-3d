{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeApplications    #-}

module Blocks.Blocks3d.Types
  ( Parity (..)
  , parityFromIntegral
  , flipParity
  , BlockTableKey' (..)
  , BlockTableKey
  ) where

import           Data.Aeson            (FromJSON (..), ToJSON (..))
import           Data.Binary           (Binary)
import           Data.Rendered         (Rendered)
import qualified Data.Set              as Set
import           GHC.Generics          (Generic)
import           Hyperion.Static       (Dict (..), Static (..), cPtr, ptrAp)
import           Blocks           (Coordinate, CoordinateDir (YDir), Sign)
import           Bootstrap.Math.HalfInteger (HalfInteger)
import           Type.Reflection       (Typeable)

data Parity = ParityEven | ParityOdd
  deriving (Eq, Ord, Show, Bounded, Enum, Generic, Binary)

instance Static (Binary Parity) where closureDict = cPtr (static Dict)

parityToInt :: Parity -> Int
parityToInt ParityEven = 0
parityToInt ParityOdd  = 1

parityFromIntegral :: Integral a => a -> Parity
parityFromIntegral i = if even i then ParityEven else ParityOdd

instance ToJSON Parity where
  toJSON = toJSON . parityToInt

instance FromJSON Parity where
  parseJSON = fmap (parityFromIntegral @Int) . parseJSON

flipParity :: Parity -> Parity
flipParity ParityEven = ParityOdd
flipParity ParityOdd  = ParityEven

-- | A type for the arguments to blocks_3d that affect the content of
-- the output (as opposed to num-threads which only affects how the
-- output happens).
--
-- Here, the type 'j' of 'jInternal' should be 'Set HalfInteger' when
-- passed to blocks_3d, but 'HalfInteger' when referring to a specific
-- block table written by blocks_3d. The Functor instance lets us
-- access the jInternal field.
data BlockTableKey' j = BlockTableKey
  { jExternal     :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
  , jInternal     :: j
  , j12           :: HalfInteger
  , j43           :: HalfInteger
  , delta12       :: Rendered Rational
  , delta43       :: Rendered Rational
  , delta1Plus2   :: Rendered Rational
  , fourPtStruct  :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
  , fourPtSign    :: Sign 'YDir
  , order         :: Int
  , lambda        :: Int
  , coordinates   :: Set.Set Coordinate
  , keptPoleOrder :: Int
  , precision     :: Int
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON, Functor)

type BlockTableKey = BlockTableKey' HalfInteger

instance (Typeable j, Static (Binary j)) => Static (Binary (BlockTableKey' j)) where
  closureDict = static (\Dict -> Dict) `ptrAp` closureDict @(Binary j)

