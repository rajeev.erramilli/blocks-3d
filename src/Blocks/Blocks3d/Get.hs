{-# LANGUAGE ApplicativeDo         #-}
{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE InstanceSigs          #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Blocks.Blocks3d.Get where

import           Control.Exception                      (Exception, throw)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           Data.Foldable                          (toList)
import qualified Data.Foldable                          as Foldable
import           Data.Functor.Compose                   (Compose (..))
import qualified Data.Map.Strict                        as Map
import qualified Data.Matrix                            as Matrix
import           Data.Maybe                             (fromMaybe)
import           Data.Proxy                             (Proxy (..))
import           Data.Reflection                        (reflect)
import           Data.Rendered                          (mkRendered)
import qualified Data.Set                               as Set
import           Data.Tagged                            (Tagged, untag)
import qualified Data.Vector                            as V
import           GHC.Generics                           (Generic)
import           Blocks                            (Block (..), BlockBase,
                                                         BlockFetchContext,
                                                         BlockTableParams,
                                                         ContinuumBlock (..),
                                                         Coordinate (..),
                                                         CoordinateDir (..),
                                                         Delta (..),
                                                         Derivative (..),
                                                         HasBlocks,
                                                         IsolatedBlock (..),
                                                         KnownCoordinate (..),
                                                         Sign, zzbDerivsAll)
import           Blocks.Blocks3d.ReadTable         (BlockTable (..),
                                                         DerivMatrix (..),
                                                         DerivMatrixKey (..))
import           Blocks.Blocks3d.Types             (BlockTableKey, Parity,
                                                         parityFromIntegral)
import qualified Blocks.Blocks3d.Types             as Key
import           Blocks.Blocks3d.WriteTable        (blockTableFilePath)
import           Blocks.ScalarBlocks.Types         (FourRhoCrossing)
import qualified Blocks.Sign                       as Sign
import           Bootstrap.Bounds.Crossing.CrossingEquations (ThreePointStructure (..))
import           Bootstrap.Build                             (Fetches, fetch)
import qualified Bootstrap.Math.DampedRational               as DR
import           Bootstrap.Math.HalfInteger                  (HalfInteger (..),
                                                         fromHalfInteger)
import qualified Bootstrap.Math.HalfInteger                  as HalfInteger
import           Bootstrap.Math.Util                         (choose, pochhammer)

-- TODO: Is there a principled way to avoid this monstrosity?
import           Blocks.ScalarBlocks.Get           (highPrecisionToString)

{- |

Conventions for conformal blocks. For conventions in 1907.11247 (see
app. A.5), the following reality properties hold:

1. OPE coeff are real if all 3 ops are bosonic and pure imaginary if
   any 2 are fermionic

2. The four-pt functions are real if there are 0 or 4 fermions. They
   are imaginary if there are 2 fermions.

3. This implies that the conformal blocks are real for bosonic
   exchanges and imaginary for fermionic exchanges.

We change this convention as follows.

1'. OPE coeffs are always real. This is achieved by lambda_there = i
   lambda_here, when there are 2 fermionic operators. Otherwise
   lambda_there = lambda_here.

2'. Four-pt functions are always real. This is achieved by 4pt_there =
   i 4pt_here when there are 2 fermionic operators in the 4pt, and
   4pt_there = 4pt_here otherwise.

These relations are nice because they are permutation-invariant. This
implies that conformal blocks are always real.

The relation between conformal blocks there and conformal blocks here
is thus as follows.

If there are 0 external fermions, the blocks are equal. If there are 4
external fermions, the blocks differ by a sign. If there are 2
external ferimions, then

Fermionic exchange:
  block_here = (-1 from 2 3pts) (i^(-1) from 4pt) block_there
Bosonic exchange
  block_here = (i from 1 3pts) (i^(-1) from 4pt) block_there

blocks_3d computes block_there for bosonic exchange and i^(-1)
block_there for fermionic exchange. This means

Fermionic exchange:
  block_here = (-1 from 2 3pts) (i^(-1) from 4pt) (i from block_3d def) blocks_3d = - blocks_3d
Bosonic exchange
  block_here = (i from 1 3pts) (i^(-1) from 4pt) blocks_3d = blocks_3d

So it seems we just need to multiply blocks_3d result by -1 for
fermionic exchanges or when there are 4 fermions. This is taken
care of in getShiftAndBlockVector for blocks loaded from blocks_3d.

Identity block generator follows these conventions.

-}

data SO3Struct a1 a2 a3 = SO3Struct
  { operator1  :: ConformalRep a1
  , operator2  :: ConformalRep a2
  , operator3  :: ConformalRep a3
  , structj12  :: HalfInteger
  , structj123 :: HalfInteger
  }
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

-- TODO: just put this inside SO3Struct
data SO3StructLabel = SO3StructLabel
  { structj12'  :: HalfInteger
  , structj123' :: HalfInteger
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

instance ThreePointStructure (SO3Struct a a a') SO3StructLabel (ConformalRep a) (ConformalRep a') where
  makeStructure o1 o2 o3 (SO3StructLabel j12 j123) =
    SO3Struct o1 o2 o3 j12 j123

data Q4Struct = Q4Struct
  { q4qs   :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
  , q4Sign :: Sign 'YDir
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

data ConformalRep a = ConformalRep
  { delta :: a
  , spin  :: HalfInteger
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

newtype Block3d = Block3d
  { unBlock3d :: Block (SO3Struct Rational Rational Delta) Q4Struct
  } deriving (Eq, Ord, Show, Generic, Binary)

newtype IdentityBlock3d = IdentityBlock3d
  { unIdentityBlock3d :: Block (ConformalRep Rational) Q4Struct
  } deriving (Eq, Ord, Show, Generic, Binary)

data Block3dParams = Block3dParams
  { nmax          :: Int
  , order         :: Int
  , keptPoleOrder :: Int
  , precision     :: Int
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

data Block3dException = Block3dException
  { exceptionMsg     :: String
  , exceptionBlock   :: Block3d
  , exceptionParams  :: Block3dParams
  , exceptionCoord   :: Coordinate
  , exceptionKey     :: BlockTableKey
  , exceptionKeyPath :: FilePath
  } deriving (Show, Exception)

throwBlock3dException :: Coordinate -> Block3dParams -> Block3d -> String -> a
throwBlock3dException coord params block msg = throw $ Block3dException
  { exceptionMsg = msg
  , exceptionBlock = block
  , exceptionParams = params
  , exceptionCoord = coord
  , exceptionKey = key
  , exceptionKeyPath = blockTableFilePath "block_dir" key
  }
  where
    key = blockToBlockTableKey coord params block

type instance BlockFetchContext Block3d a m = Fetches BlockTableKey (BlockTable a) m
type instance BlockTableParams Block3d = Block3dParams

instance (Eq a, RealFloat a, KnownCoordinate c) => IsolatedBlock Block3d (Derivative c) a where
  getBlockIsolated d b = isolateBlock $ getShiftAndBlockVector d b

type instance BlockFetchContext IdentityBlock3d a m = ()
type instance BlockTableParams IdentityBlock3d = Block3dParams

instance (Eq a, RealFloat a, KnownCoordinate c) => IsolatedBlock IdentityBlock3d (Derivative c) a where
  getBlockIsolated
    :: forall p m v. (HasBlocks IdentityBlock3d p m a, Applicative v, Foldable v)
    => v (Derivative c)
    -> IdentityBlock3d
    -> Compose (Tagged p) m (v a)
  getBlockIsolated derivs block@(IdentityBlock3d (Block {fourPtFunctional = Q4Struct{ q4Sign }})) =
    pure $ fmap (getDeriv q4Sign derivativeMap) derivs
    where
      derivativeMap = fromZZb q4Sign nmax' (getIdentityBlockZZB nmax' block)
      nmax' = nmax $ reflect @p Proxy
      -- This hack, suggested here
      -- (https://ghc.readthedocs.io/en/8.0.1/using-warnings.html#ghc-flag--Wredundant-constraints)
      -- suppresses the redundant constraint warning for Foldable
      _ = Foldable.length derivs

type instance BlockBase Block3d a = FourRhoCrossing a

instance (Eq a, RealFloat a, KnownCoordinate c) => ContinuumBlock Block3d (Derivative c) a where
  getBlockContinuum d b = gapBlock $ getShiftAndBlockVector d b

-- | WARNING: This function uses 'highPrecisionToString' on all
-- Rationals, which truncates the precision to a fixed value. We
-- should probably change this.
blockToBlockTableKey :: Coordinate -> Block3dParams -> Block3d -> BlockTableKey
blockToBlockTableKey coordinate params block = case (params, block) of
  ( Block3dParams { nmax, order, keptPoleOrder, precision }
    , Block3d
      (Block
       { struct12         = SO3Struct (ConformalRep delta1 j1) (ConformalRep delta2 j2) (ConformalRep _ j) j12 _
       , struct43         = SO3Struct (ConformalRep delta4 j4) (ConformalRep delta3 j3) (ConformalRep _ j') j43 _
       , fourPtFunctional = Q4Struct qs s
       })
    ) -> if j == j' then
    Key.BlockTableKey
    { Key.jExternal     = (j1, j2, j3, j4)
    , Key.jInternal     = j
    , Key.j12           = j12
    , Key.j43           = j43
    , Key.delta12       = mkRendered highPrecisionToString $ delta1 - delta2
    , Key.delta43       = mkRendered highPrecisionToString $ delta4 - delta3
    , Key.delta1Plus2   = mkRendered highPrecisionToString $ delta1 + delta2
    , Key.fourPtStruct  = qs
    , Key.fourPtSign    = s
    , Key.order         = order
    , Key.lambda        = 2*nmax - 1
    , Key.coordinates   = Set.singleton coordinate
    , Key.keptPoleOrder = keptPoleOrder
    , Key.precision     = precision
    }
    else error $ "Mismatched internal operators in block " ++ show block

getBlockTable
  :: forall p m c a . (HasBlocks Block3d p m a, KnownCoordinate c)
  => Block3d
  -> Tagged c (Compose (Tagged p) m (BlockTable a))
getBlockTable = pure . Compose . pure . fetch .
  blockToBlockTableKey (reflectCoordinate @c Proxy) (reflect @p Proxy)

unitarityBound :: HalfInteger -> Rational
unitarityBound j = if
  | j == 0    -> 1/2
  | j == 1/2  -> 1
  | otherwise -> fromHalfInteger j + 1

fixedDelta :: ConformalRep Delta -> Rational
fixedDelta rep = case delta rep of
  Fixed d             -> d
  RelativeUnitarity x -> unitarityBound (spin rep) + x

threePtParity :: SO3Struct a1 a2 a3 -> Parity
threePtParity (SO3Struct (ConformalRep _ j1) (ConformalRep _ j2) (ConformalRep _ j) _ j120) =
  case HalfInteger.toInteger (j1 - j2 + j - j120) of
    Just i -> parityFromIntegral i
    Nothing -> error $
      "j1 - j2 + j - j120 should be an integer, but found: " ++
      show (j1 - j2 + j - j120)

getShiftAndBlockVector
  :: forall p m a c v. (HasBlocks Block3d p m a, Functor v, Eq a, Fractional a, KnownCoordinate c)
  => v (Derivative c)
  -> Block3d
  -> Compose (Tagged p) m (Rational, DR.DampedRational (FourRhoCrossing a) v a)
getShiftAndBlockVector derivVec b'@(Block3d b) = do
  bt <- untag @c (getBlockTable b')
  pure $
    let
      throwErr :: forall e . String -> e
      throwErr = throwBlock3dException (reflectCoordinate @c Proxy) (reflect @p Proxy) b'
      SO3Struct _ _ _ _ j120 = struct12 b
      SO3Struct _ _ _ _ j430 = struct43 b
      internalRep = if operator3 (struct12 b) == operator3 (struct43 b)
        then
          operator3 (struct12 b)
        else
          throwErr $ "Mismatched internal operators in block " ++ show b'
      xShift = fixedDelta internalRep - deltaMinusX bt
      coord = reflectCoordinate @c Proxy
      p12 = threePtParity (struct12 b)
      p43 = threePtParity (struct43 b)
      DerivMatrix j120s j430s derivs = case derivMatrices bt Map.!? DerivMatrixKey p12 p43 coord of
        Just v -> v
        Nothing -> throwErr $ "derivMatrices does not contain key " ++ show (DerivMatrixKey p12 p43 coord)
      drDerivMap = case (V.elemIndex j120 j120s, V.elemIndex j430 j430s) of
        (Just i, Just j) -> DR.mapNumerator ((Matrix.! (i+1,j+1)) . getCompose) derivs
        _ -> throwErr "Block table does not contain the given (j120, j430)"
      needSign = not (HalfInteger.isInteger (spin internalRep))
                 || all (not . HalfInteger.isInteger)
                    [ spin . operator1 . struct12 $ b
                    , spin . operator2 . struct12 $ b
                    , spin . operator1 . struct43 $ b
                    , spin . operator2 . struct43 $ b
                    ]
      -- The two cases here take care of our block conventions as compared to blocks_3d
      getDeriv' m d = case m Map.!? d of
        Just v  -> v
        Nothing -> throwErr $ "drDerivMap does not contain key " ++ show d
      drVec =
        if not needSign
        then
          DR.mapNumerator (\m -> fmap (\(Derivative d) -> getDeriv' m d) derivVec) drDerivMap
        else
          DR.mapNumerator (\m -> fmap (\(Derivative d) -> -(getDeriv' m d)) derivVec) drDerivMap
    in (xShift, drVec)

gapBlock 
  :: (Functor m, Functor v, Floating a, Eq a)
  => Compose (Tagged p) m (Rational, DR.DampedRational (FourRhoCrossing a) v a)
  -> Compose (Tagged p) m (DR.DampedRational (FourRhoCrossing a) v a)
gapBlock d = uncurry DR.shift <$> d

isolateBlock
  :: (Functor m, Functor v, Foldable v, RealFloat a)
  => Compose (Tagged p) m (Rational, DR.DampedRational (FourRhoCrossing a) v a)
  -> Compose (Tagged p) m (v a)
isolateBlock d = eval <$> d
  where 
    eval (x, v) = DR.evalCancelPoleZeros (fromRational x) v

getIdentityBlockZZB
  :: forall a. Floating a => Int -> IdentityBlock3d -> Map.Map (Derivative 'ZZb) a
getIdentityBlockZZB nmax (IdentityBlock3d b) =
  Map.fromSet compute (Set.fromList (toList (zzbDerivsAll nmax)))
  where
    ConformalRep delta1 j1 = struct12 b
    ConformalRep _      j3 = struct43 b
    Q4Struct (q1,q2,q3,q4) s = fourPtFunctional b
    compute (Derivative (m,n)) = (compute' (m,n) + Sign.toNum s * compute' (n,m))/2
    compute' :: (Int, Int) -> a
    compute' (m,n) =
      if q1==q2 && q3==q4 then
        normalization * (2::a)**(fromRational $ (fromIntegral m)+(fromIntegral n)+2*delta1)
          * (-1)^(m+n) * fromRational (pochhammer (delta1-fromHalfInteger q1) m
            * pochhammer (delta1+fromHalfInteger q1) n)
      else 0
    normalization = (-1)^(j1mq1+j3mq3) *
      case (HalfInteger.toInteger j1, HalfInteger.toInteger j3) of
        (Just j1', Just j3') -> (-1)^(j1'+j3')
        (Just j1', Nothing)  -> (-1)^(j1' + (twice j3 - 1) `div` 2)
        (Nothing, Just j3')  -> (-1)^(j3' + (twice j1 - 1) `div` 2)
        (Nothing, Nothing)   -> (-1)^((twice j1 + twice j3) `div` 2)
      * fromInteger ((choose (twice j1) j1mq1) * (choose (twice j3) j3mq3))
    j1mq1 = fromMaybe (error err) $ HalfInteger.toInteger $ j1-q1
    j3mq3 = fromMaybe (error err) $ HalfInteger.toInteger $ j3-q3
    err = "The difference j-q must be an integer in getIdentityBlockZZB"
